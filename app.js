const calcular = () => {
    numero1 = parseFloat(document.getElementById("number1").value);
    numero2 = parseFloat(document.getElementById("number2").value);
    if(isNaN(numero1)||isNaN(numero2)){
        document.getElementById("resCalcular").innerHTML = `<p>Debes ingresar un valor</p>`;
    }else{
        document.getElementById("resCalcular").innerHTML = `<p>La suma es: ${numero1 + numero2} </p>
                                                        <p> La resta es: ${numero1 - numero2} </p>
                                                        <p>La multiplicación es: ${numero1 * numero2} </p>
                                                        <p>La división es: ${numero1 / numero2} </p>`;
    }
}


const convertirOctal = () => {
    let octal = parseInt(document.getElementById("octal").value);
    if (isNaN(octal)) {
        document.getElementById("resOctal").innerHTML = `<p>Debes ingresar un valor</p>`;
    } else {
        octal = parseInt(octal, 8);
        document.getElementById("resOctal").innerHTML = `<p>El resultado es: ${octal} </p>`;
    }
};